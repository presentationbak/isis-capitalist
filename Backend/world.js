module.exports = {
    "name": "Sonic",
    "logo": "icones/logo.jpg",
    "money": 0,
    "score": 0,
    "totalangels": 0,
    "activeangels": 0,
    "angelbonus": 0,
    "lastupdate": 0,
    "products": [
        {
            "id": 1,
            "name": "Rings Generator",
            "logo": "icones/ringsgenerator.jpg",
            "cout": 4,
            "croissance": 1.07,
            "revenu": 1,
            "vitesse": 500,
            "quantite": 1,
            "timeleft": 0,
            "managerUnlocked": false,
            "palliers": [
                {
                    "name": "Green Hill Zone",
                    "logo": "icones/greenhillzone.jpg",
                    "seuil": 20,
                    "idcible": 1,
                    "ratio": 2,
                    "typeratio": "vitesse",
                    "unlocked": false
                },
                {
                    "name": "Chemical Plant Zone",
                    "logo": "icones/chemicalplantzone.png",
                    "seuil": 75,
                    "idcible": 1,
                    "ratio": 2,
                    "typeratio": "vitesse",
                    "unlocked": false
                }
            ]
        },
        {
            "id": 2,
            "name": "Tails' Tech Factory",
            "logo": "icones/tailstechfactory.jpg",
            "cout": 0,
            "croissance": 1.1,
            "revenu": 0,
            "vitesse": 1000,
            "quantite": 1,
            "timeleft": 0,
            "managerUnlocked": false,
            "palliers": [
                {
                    "name": "Sky Sanctuary",
                    "logo": "icones/skysanctuary.png",
                    "seuil": 0,
                    "idcible": 2,
                    "ratio": 2,
                    "typeratio": "revenu",
                    "unlocked": false
                },
                {
                    "name": "Angel Island",
                    "logo": "icones/angelisland.jpg",
                    "seuil": 150,
                    "idcible": 2,
                    "ratio": 2,
                    "typeratio": "revenu",
                    "unlocked": false
                }
            ]
        },
     
        {
            "id": 3,
            "name": "Knuckles' Master Emerald Mine",
            "logo": "icones/knucklesmine.png",
            "cout": 500,
            "croissance": 1.15,
            "revenu": 0,
            "vitesse": 1500,
            "quantite": 1,
            "timeleft": 0,
            "managerUnlocked": false,
            "palliers": [
                {
                    "name": "Mystic Ruins",
                    "logo": "icones/mysticruins.jpg",
                    "seuil": 100,
                    "idcible": 3,
                    "ratio": 2,
                    "typeratio": "revenu",
                    "unlocked": false
                },
                {
                    "name": "Hidden Palace Zone",
                    "logo": "icones/hiddenpalacezone.png",
                    "seuil": 250,
                    "idcible": 3,
                    "ratio": 2,
                    "typeratio": "revenu",
                    "unlocked": false
                }
            ]
        },
        {
            "id": 4,
            "name": "Amy's Hammer Shop",
            "logo": "icones/amyhammer.jpg",
            "cout": 2000,
            "croissance": 1.2,
            "revenu": 0,
            "vitesse": 2000,
            "quantite": 1,
            "timeleft": 0,
            "managerUnlocked": false,
            "palliers": [
                {
                    "name": "Twinkle Park",
                    "logo": "icones/twinklepark.png",
                    "seuil": 500,
                    "idcible": 4,
                    "ratio": 2,
                    "typeratio": "revenu",
                    "unlocked": false
                },
                {
                    "name": "Casinopolis",
                    "logo": "icones/casinopolis.jpg",
                    "seuil": 1000,
                    "idcible": 4,
                    "ratio": 2,
                    "typeratio": "revenu",
                    "unlocked": false
                }
            ]
        },
        {
            "id": 5,
            "name": "Robotnik's Mean Bean Machine Factory",
            "logo": "icones/hyperknucklesupgrade.png",
            "cout": 10000,
            "croissance": 1.25,
            "revenu": 0,
            "vitesse": 3000,
            "quantite": 1,
            "timeleft": 0,
            "managerUnlocked": false,
            "palliers": [
                {
                    "name": "Scrap Brain Zone",
                    "logo": "icones/scrapbrainzone.jpg",
                    "seuil": 2000,
                    "idcible": 5,
                    "ratio": 2,
                    "typeratio": "revenu",
                    "unlocked": false
                },
                {
                    "name": "Lava Reef Zone",
                    "logo": "icones/lavareefzone.png",
                    "seuil": 5000,
                    "idcible": 5,
                    "ratio": 2,
                    "typeratio": "revenu",
                    "unlocked": false
                }
            ]
        },
        {
            "id": 6,
            "name": "Fire Flower Farm",
            "logo": "icones/fireflowerfarm.png",
            "cout": 15,
            "croissance": 1.12,
            "revenu": 0,
            "vitesse": 600,
            "quantite": 1,
            "timeleft": 0,
            "managerUnlocked": false,
            "palliers": [
                {
                    "name": "Mushroom Kingdom Meadows",
                    "logo": "mushroomkingdommeadows.png",
                    "seuil": 40,
                    "idcible": 6,
                    "ratio": 2.8,
                    "typeratio": "revenu",
                    "unlocked": false
                },
                {
                    "name": "Bowser's Castle Blooms",
                    "logo": "icones/bowserscastleblooms.jpg",
                    "seuil": 100,
                    "idcible": 6,
                    "ratio": 3.5,
                    "typeratio": "revenu",
                    "unlocked": false
                }
            ]
        },
    ],
        "allunlocks": [
            {
                "name": "Super Sonic Level",
                "logo": "icones/supersonicunlocks.jpg",
                "seuil": 30,
                "idcible": 0,
                "ratio": 2,
                "typeratio": "revenu",
                "unlocked": false
            }
        ],
        "upgrades": [
            {
                "name": "Hyper Knuckles Upgrade",
                "logo": "hyperknucklesupgrade.png",
                "seuil": 1e3,
                "idcible": 1,
                "ratio": 3,
                "typeratio": "revenu",
                "unlocked": false
            }
        ],
        "angelupgrades": [
            {
                "name": "Chaos Emerald Boost",
                "logo": "icones/chaosemeraldboost.png",
                "seuil": 10,
                "idcible": 0,
                "ratio": 3,
                "typeratio": "revenu",
                "unlocked": false
            }
        ],
        "managers": [
            {
                "name": "Tails",
                "logo": "icones/tails.png",
                "seuil": 10,
                "idcible": 1,
                "ratio": 0,
                "typeratio": "revenu",
                "unlocked": false
            },
        
            {
                "name": "Sonic",
                "logo": "icones/sonic.jpg",
                "seuil": 30,
                "idcible": 3,
                "ratio": 2,
                "typeratio": "revenu",
                "unlocked": false
            },
            {
                "name": "Amy",
                "logo": "icones/amy.jpg",
                "seuil": 40,
                "idcible": 4,
                "ratio": -1,  // Dr. Robotnik pourrait diminuer le revenu !
                "typeratio": "revenu",
                "unlocked": false
            }
        ],
       
    };
    
