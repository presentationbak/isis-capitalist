import {gql} from "@urql/core";

export const BACKEND = "http://localhost:4000/"
// https://isiscapitalistgraphql.kk.kurasawa.fr/graphql
// http://localhost:4000/graphql

export const GET_WORLD = gql`
query getWorld { 
    getWorld { 
      name money
      upgrades {
        idcible
        logo
        name
        ratio
        seuil
        typeratio
        unlocked
      }
      products {
        cout
        croissance
        id
        logo
        managerUnlocked
        name
        paliers {
          idcible
          logo
          name
          ratio
          seuil
          typeratio
          unlocked
        }
        quantite
        revenu
        timeleft
        vitesse
      }
      managers {
        idcible
        logo
        name
        ratio
        seuil
        typeratio
        unlocked
      }
      logo
      lastupdate
      angelupgrades {
        idcible
        logo
        name
        ratio
        seuil
        typeratio
        unlocked
      }
      angelbonus
      allunlocks {
        idcible
        logo
        name
        ratio
        seuil
        typeratio
        unlocked
      }
      activeangels
      score
      totalangels
    } 
}`


export const ACHETER_QT_PRODUIT = gql`
  mutation acheterQtProduit($id: Int!, $quantite: Int!) {
    acheterQtProduit(id: $id, quantite: $quantite) {
      id
      quantite
    }
  }
`;

export const LANCER_PRODUCTION = gql`
  mutation lancerProductionProduit($id: Int!) {
    lancerProductionProduit(id: $id) {
      id
    }
  }
`;

export const ENGAGER_MANAGER = gql`
  mutation engagerManager($name: String!) {
    engagerManager(name: $name) {
      name
    }
  }
`;

export const ACHETER_CASHUPGRADE = gql`
  mutation acheterCashUpgrade($name: String!) {
    acheterCashUpgrade(name: $name) {
      name
    }
  }
`;

export const ACHETER_ANGELUPGRADE = gql`
  mutation acheterAngelUpgrade($name: String!) {
    acheterAngelUpgrade(name: $name) {
      name
    }
  }
`;

export const RESET_WORLD = gql`
  mutation resetWorld {
    resetWorld {
      name
      logo
      money
      score
      totalangels
      activeangels
      angelbonus
      lastupdate
      products {
        id
        name
        logo
        cout
        croissance
        revenu
        vitesse
        quantite
        timeleft
        lastupdate
        managerUnlocked
        paliers {
          name
          logo
          seuil
          idcible
          ratio
          typeratio
          unlocked
        }
      }
      allunlocks {
        name
        logo
        seuil
        idcible
        ratio
        typeratio
        unlocked
      }
      upgrades {
        name
        logo
        seuil
        idcible
        ratio
        typeratio
        unlocked
      }
      angelupgrades {
        name
        logo
        seuil
        idcible
        ratio
        typeratio
        unlocked
      }
      managers {
        name
        logo
        seuil
        idcible
        ratio
        typeratio
        unlocked
      }
    }
  }
`;