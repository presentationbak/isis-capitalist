import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

import { Palier, World } from '../world';
import { WebserviceService } from '../webservice.service';
import { CommonModule } from '@angular/common'; // Assurez-vous d'importer CommonModule

@Component({
  selector: 'app-pop-up-managers',
  standalone: true,
  imports: [CommonModule], // Supprimez MatSnackBar d'ici
  templateUrl: './pop-up-managers.component.html',
  styleUrl: './pop-up-managers.component.css'
})
export class PopUpManagersComponent {

  // Côté serveur
  server: string = this.data.server;

  // Monde
  world: World = this.data.world;

  showManagers: Boolean = false;

  // Si pas de manager
  msgNoManager: string = "";

  // Output
  @Output()
  update: EventEmitter<Palier> = new EventEmitter<Palier>();

  // Constructeur
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private restService: WebserviceService, private snackBar: MatSnackBar) {};

  // Méthodes
  // Embaucher un manager
  hireManager(manager: Palier) {
    this.restService.engagerManager(manager).catch(reason =>
      console.log("Erreur : " + reason)
    );

    if(this.world.money >= manager.seuil) {
      this.world.money-= manager.seuil;
      manager.unlocked = true;
      this.world.products[manager.idcible - 1].managerUnlocked = true;
      this.update.emit(manager);
      this.showSnackBar('Nouveau manager engagé avec suycès !');
    }

    if(this.world.managers.filter(m => !m.unlocked).length == 0) {
      this.msgNoManager = "Oh non, il n'y plus de managers à embaucher !";
    }
  }

  showSnackBar(message: string): void {
    this.snackBar.open(message, '', {
      duration: 2000, // Durée d'affichage du message en millisecondes (2 secondes)
      panelClass: ['snackbar'] // Classe CSS personnalisée pour la Snackbar
    });
  }

}
