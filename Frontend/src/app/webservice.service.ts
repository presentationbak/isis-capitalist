import { Injectable } from '@angular/core';
import { Client, fetchExchange } from '@urql/core';
import { GET_WORLD, BACKEND} from './request';
import { ACHETER_ANGELUPGRADE, ACHETER_CASHUPGRADE, ACHETER_QT_PRODUIT, ENGAGER_MANAGER,  LANCER_PRODUCTION, RESET_WORLD } from './request';
import { Palier, Product } from './world';
@Injectable({
  providedIn: 'root'
})
export class WebserviceService {
  // https://isiscapitalistgraphql.kk.kurasawa.fr/graphql
  // http://localhost:4000/graphql
  server = BACKEND ; 
  user ='Marc';

  constructor() { }

  createClient() { 
    return new Client({ url: this.server + "graphql",
       exchanges: [fetchExchange],
       fetchOptions: () => { return { 
        headers: {'x-user': this.user}, 
      }; 
    }, }); 
  }
  

  
    setUser(user: string ) {
      this.user = user;
    }
  
    getWorld() {
      return this.createClient().query(GET_WORLD, {}).toPromise();
    }
  
    acheterQtProduit(product: Product) {
      return this.createClient().mutation(ACHETER_QT_PRODUIT, { id: product.id, quantite: product.quantite}).toPromise();
    }
  
    lancerProductionProduit(product: Product) {
      return this.createClient().mutation(LANCER_PRODUCTION, { id: product.id}).toPromise();
    }
 
  
    engagerManager(manager: Palier) {
      return this.createClient().mutation(ENGAGER_MANAGER, { name: manager.name}).toPromise();
    }
  
    acheterCashUpgrade(upgrade: Palier) {
      return this.createClient().mutation(ACHETER_CASHUPGRADE, { name: upgrade.name}).toPromise();
    }
  
    acheterAngelUpgrade(upgrade: Palier) {
      return this.createClient().mutation(ACHETER_ANGELUPGRADE, { name: upgrade.name}).toPromise();
    }
  
    resetWorld() {
      return this.createClient().mutation(RESET_WORLD, {}).toPromise();
    }
    

}


