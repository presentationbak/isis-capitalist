import { Component, QueryList, ViewChildren, afterNextRender } from '@angular/core';
// import { RouterOutlet } from '@angular/router';
import { CommonModule } from '@angular/common';
import { Palier, World, Product } from './world';

import { WebserviceService } from './webservice.service';
import { BigvaluePipe } from './bigvalue.pipe';
import { ProductComponent } from './product/product.component';

import { PopUpManagersComponent } from './pop-up-managers/pop-up-managers.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatBadgeModule } from '@angular/material/badge';

import { PopUpUnlocksComponent } from './pop-up-unlocks/pop-up-unlocks.component';
import { MatDialog } from '@angular/material/dialog';
import { PopUpUpgradesComponent } from './pop-up-upgrades/pop-up-upgrades.component';
import { PopUpAngelsComponent } from './pop-up-angels/pop-up-angels.component';

import { BarComponent } from './bar/bar.component';
import { MatBadge } from '@angular/material/badge';
import { Console } from 'node:console';
import { FormsModule } from '@angular/forms';


@Component({
  selector: 'app-root',
  standalone: true,
  imports: [ BigvaluePipe,ProductComponent,CommonModule, MatBadge, BigvaluePipe,MatBadgeModule, FormsModule],
  templateUrl: 'app.component.html',
  styleUrl: 'app.component.css'
})

export class AppComponent {
  title = 'Frontend';
  server!: string;

  world: World = new World();

  qtmulti!: string;
  quantites: string[] =  ["1", "10", "100", "MAX"];
  username: string = "";

  // Badges des boutons
  badgeUpgrades: number = 0;
  badgeAngelUpgrades: number = 0;
  badgeManagers: number = 0;

  // Temps écoulé
  tempsEcoule: boolean = false;

  // ViewChildren
  @ViewChildren(ProductComponent)
   productsComponent!: QueryList<ProductComponent>;

  // Constructeur
  constructor(
    private restService: WebserviceService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,
    

  

    ) { 

      afterNextRender(() => {
        // Focus the first input element in this component.
        this.username = localStorage.getItem("username") || "";
        console.info("username: ", this.username)
      });
           
    }

  // Méthode
  ngOnInit() {
    // this.calcTempsEcoule()dfdsjfhdnjhn
   
    this.qtmulti = this.quantites[0];

    // this.updateUsername();
        
    this.restService.getWorld().then( world => {
      this.world = world.data.getWorld;
      console.log( "L'argent du monde : ",this.world.money)
      this.server = this.restService.server
      
    });

  }

  calcTempsEcoule() {
    setTimeout(() => {
        if(!this.server) {
          this.tempsEcoule = true;
        }
    }, 5000);
  }


  // changeMultiplicateur() {
  //   let quantite: string = this.quantites[this.quantites.indexOf(this.qtmulti) + 1];
    
  //   this.qtmulti = quantite == undefined ? this.quantites[0] : quantite;
  // }

 

  // Produire
  onProductionDone(eventData: {product: Product, nbProduction: number}) {
    console.log("Production faite : ", eventData.nbProduction)
    console.log(" Argent du mon monde  : ",  this.world.money)
      this.world.money += eventData.product.revenu * eventData.product.quantite * (1 + this.world.activeangels * this.world.angelbonus / 100);
      this.world.score += eventData.product.revenu * eventData.product.quantite * (1 + this.world.activeangels * this.world.angelbonus / 100);
      console.log("argent du monde apres ajout : ", this.world.money)
      this.updateBadges();
  }

   // Acheter + Contrôler les Unlocks + Upgrades + Angels Upgrades
   onBuy(eventData: {product: Product, coutProduct: number}) {
      console.log("Le cout à enlever : ", eventData.coutProduct)
      console.log("Argent du monde ici : ", this.world.money)
      this.world.money -= eventData.coutProduct
      this.updateBadges();

    let allUnlocks: Palier[] = this.world.allunlocks.filter(a => a.idcible == 0);
    let unlocksProduct: Palier[] = this.world.allunlocks.filter(a => a.idcible == eventData.product.id);

    allUnlocks.forEach(a => {
      let quantiteMin: number = 0;
      this.world.products.forEach((p, index) => {
        if(index == 0 || p.quantite < quantiteMin) {
          quantiteMin = p.quantite;
        }
      })

      if (quantiteMin >= a.seuil) {
        this.productsComponent.forEach(p => p.calcUpgrade(a));
        this.openSnackBar("AllUnlock (x" + a.ratio + " " + a.typeratio + ") pour tous les produits a été débloqué !", 'Info');
      }
    })

    unlocksProduct.forEach(u => {
      if (eventData.product.quantite >= u.seuil) {
        this.productsComponent.toArray()[u.idcible - 1].calcUpgrade(u);
        this.openSnackBar("Unlock (x" + u.ratio + " " + u.typeratio + ") du produit \"" + eventData.product.name + "\" a été débloqué !", 'Info');
      }
    });
    
  }





  // Changer de nom d'utilisateur
  // onUsernameChanged() {
  //  localStorage.setItem("username", this.username);
  //  this.restService.setUser(this.username);
  //  window.location.reload();
  // }

  //Afficher un SnackBar
  openSnackBar(_message: string, _type: string) {
    // this._snackBar.openFromComponent(SnackBarComponent, {
    //   duration: 5000,
    //   horizontalPosition: 'end',
    //   verticalPosition: 'top',
    //   data: { message: message, snackType: type }
    // });
  }

  // Afficher le pop-up des Unlocks
  popUpUnlocks() {
    this.dialog.open(PopUpUnlocksComponent, {
      data: {
        server: this.server,
        world: this.world
      }
    });
  }

  // Afficher le pop-up des Upgrades
  popUpUpgrades(type: string) {
    let typeUpgrades: Palier[] = [];
    let titre: string = '';
    let message: string = '';

    if(type != 'ANGE') {
      typeUpgrades = this.world.upgrades;
      titre = 'Cash Upgrades';
      message = 'Il faut dépenser de l\'argent pour gagner de l\'argent ! Achetez ces mises à niveau de qualité supérieure pour donner un coup de pouce à vos entreprises.';
    }
    else{
      typeUpgrades = this.world.angelupgrades;
      titre = 'Angel Upgrades';
      message = 'Il ne peut y avoir de progrès sans sacrifice. Ces mises à niveau coûtent aux investisseurs providentiels !';
    }

    const dialogRef = this.dialog.open(PopUpUpgradesComponent, {
        data: {
          server: this.server,
          world: this.world,
          upgrades: typeUpgrades,
          type: type,
          titre: titre,
          message: message
        }
      });

    dialogRef.componentInstance.update.subscribe((upgrade) => {
      this.updateBadges();

      let message: string = "";

      if(upgrade.idcible == 0) {
        this.productsComponent.forEach(p => p.calcUpgrade(upgrade));
        message = "AllUpgrade " + upgrade.name + "\" pour tous les produits a été acheté avec succès !";
      }
      else {
        this.productsComponent.toArray()[upgrade.idcible - 1].calcUpgrade(upgrade);
        message = "Upgrade " + upgrade.name + "\" du produit \"" + this.world.products[upgrade.idcible - 1].name + "\" a été acheté avec succès !";
      }
      this.openSnackBar(message, 'Success');
    });
  }

  // Afficher le pop-up des managers
  popUpManagers() {
    const dialogRef = this.dialog.open(PopUpManagersComponent, {
      data: {
        server: this.server,
        world: this.world
      }
    });

    dialogRef.componentInstance.update.subscribe((manager) => {
      this.updateBadges()
      this.openSnackBar("Le manager " + manager.name + " du produit \"" + this.world.products[manager.idcible - 1].name + "\" a été embauché avec succès !", 'Success');
    });
  }

  // Afficher le pop-up des anges
  popUpInvestors() {
    this.dialog.open(PopUpAngelsComponent, {
      data: {
        server: this.server,
        world: this.world
      }
    });
  }

  // Actualiser les badges
  updateBadges() {
    var upgrades: Palier[] = this.world.upgrades.filter(u => u.seuil <= this.world.money);
    var angelUpgrades: Palier[] = this.world.angelupgrades.filter(a => a.seuil <= this.world.activeangels);
    var managers: Palier[] = this.world.managers.filter(m => m.seuil <= this.world.money);

    this.badgeUpgrades = upgrades.filter(u => !u.unlocked).length;
    this.badgeAngelUpgrades = angelUpgrades.filter(a => !a.unlocked).length;
    this.badgeManagers = managers.filter(m => !m.unlocked).length;
  }

  onUsernameChanged( ) {
    localStorage.setItem("username", this.username);
   console.log(this.username);
    
    // this.restService.setUser(this.username);
   
    // window.location.reload();
   }

  // Mettre à jour le nom d'utilisateur
  updateUsername() {
    let userStocke = localStorage.getItem("username");

    if (!userStocke) {
      userStocke = "Doctor" + Math.floor(Math.random() * 10000);
    }

    this.username = userStocke;
    
    localStorage.setItem("username", this.username);
    this.restService.setUser(this.username);
  }


  // Méthode pour changer la position du commutateur lors d'un clic
    // Changer le multiplicateur
  changePosition() {
    let quantite: string = this.quantites[this.quantites.indexOf(this.qtmulti) + 1];
    
    this.qtmulti = quantite == undefined ? this.quantites[0] : quantite;
    // switch(this.qtmulti) {
    //   case '1':
    //     this.qtmulti = '10';
    //     break;
    //   case '10':
    //     this.qtmulti = '100';
    //     break;
    //   case '100':
    //     this.qtmulti = 'Max';
    //     break;
    //   case 'Max':
    //     this.qtmulti = '1';
    //     break;
    //   default:
    //     this.qtmulti = '1'; // Retour à la position x1 si la position actuelle n'est pas reconnue
    //     break;
    // }
  }



  }


  // À propos du jeu
  // about() {
  //   this.dialog.open(PopUpAboutComponent);
  // }

  // Afficher le message d'erreur si le chargement du serveur n'a pas fonctionné
  

  
























