
import { Component, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';

@Component({
  selector: 'app-bar',
  standalone: true,
  imports: [],
  templateUrl: './bar.component.html',
  styleUrl: './bar.component.css'
})
export class BarComponent {
  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any) { }

  get getIcon() {
    switch (this.data.snackType) {
      case 'Success':
        return 'check';
      case 'Error':
        return 'error';
      case 'Warn':
        return 'warning';
      case 'Info':
        return 'info';
      default:
        return 'info';
    }
  }
}
