import { LOCALE_ID, NgModule } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { ProductComponent } from './product/product.component';
import { MyProgressBarComponent } from './progressbar.component';
import { FormsModule } from '@angular/forms';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButton } from '@angular/material/button';
registerLocaleData(localeFr, 'fr');

@NgModule({
  declarations: [

    
  ],
  imports: [
    // BrowserAnimationsModule,
    BrowserModule,
    MatBadgeModule,
    MatButton
  ],
  providers: [{
    provide: LOCALE_ID,
    useValue: 'fr'
  }],
  bootstrap: [],
})
export class AppModule { }