import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Product, Palier, World } from '../world';
import { WebserviceService } from '../webservice.service';
import { Pipe, PipeTransform } from '@angular/core';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatGridListModule } from '@angular/material/grid-list'; 
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CommonModule } from '@angular/common';
import { MyProgressBarComponent } from '../progressbar.component';
import { Console } from 'console';

@Component({
  selector: 'app-product',
  standalone: true,
  imports: [
    MatBadgeModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatDividerModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MyProgressBarComponent,
    MatSnackBarModule,
    CommonModule
  ],
  templateUrl: './product.component.html',
  styleUrl: './product.component.css'
})
export class ProductComponent {
  server: string;
  
    product: Product = new Product() ;
    world : World = new World();
    auto: boolean = false;
    progressBarValue: number = 0;
    run: boolean = false;
    maxQuantity: number = 0 ;
    maxCout: number = 0 ; 

    @Input()
    set prod(value: Product) {
      if (!value) value = new Product()
      console.log("timeleft: " + value.timeleft)
      this.product = value;
      this.calcMaxCanBuy(); // Appel de calcMaxCanBuy() lorsque la valeur de product change
      if (this.product && this.product.timeleft > 0) {
        this.world.lastupdate = Date.now().toString();
        this.product.lastupdate = Date.now().toString();
        this.progressBarValue = ((this.product.vitesse - this.product.timeleft) / this.product.vitesse) * 100;
        this.run = true;
        this.auto = this.product.managerUnlocked;
      }
    }


  // Méthodes
  ngOnInit() {
    setInterval(() => {
      this.calcScore();
      this.calcMaxCanBuy();
      // console.log("L'argent : ", this._money)
      // console.log("multiplicateur : ", this._qtmulti)
      // console.log("quantite maximale : ", this.maxQuantity)
    }, 100);
  }
    
  _world!: World
  @Input()
  set World(value: World) {
    this._world = value;
    if(this._world && this.product) {
      this.calcMaxCanBuy();
    }
  }

  _qtmulti!: string;
  @Input()
  set qtmulti(value: string) {
  this._qtmulti = value;
  this.calcMaxCanBuy(); // Appel de calcMaxCanBuy() 
  if (this._qtmulti && this.product) this.calcMaxCanBuy();
  }


  _money!: number;
  @Input()
  set money(value: number) {
    this._money = value;
    this.calcMaxCanBuy(); // Appel de calcMaxCanBuy() 
    if(this._money && this.product) {
      console.log(this.maxQuantity)
    }
  }

  // calcMaxCanBuy() {
  //   let quantite: number = 0;
  //   console.log("multiplicateur : ", this._qtmulti)
  //   console.log("Argent : ", this._money)
  //   console.log("Croissance : ", this.product.croissance)
  //   console.log("Cout du produit : ", this.product.cout)

  //   if(Number(this._qtmulti)) {
  //     quantite = Number(this._qtmulti);
  //   }
  //   else{
  //     quantite = Math.floor(Math.log(-((this._money * (1 - this.product.croissance)) / this.product.cout) + 1) / Math.log(this.product.croissance));
  //     if(quantite <= 0){
  //       quantite = 1;
  //     }
  //   }
  //   this.maxQuantity = quantite
  //   this.product.cout * this.maxQuantity
  //   return   ;
  // }

    // Acheter le produit
    buyProduct() {
      this.restService.acheterQtProduit(this.product).catch(reason =>
        console.log("Erreur : " + reason)
      );
  
        console.log("Je suis en train d'acheter un produit l'ancienne qt:  ", this.product.quantite)
        this.product.quantite += this.maxQuantity;
        console.log("la quantite de produit : ", this.product.quantite);
        console.log("cout du produit : ", this.product.cout);
        this.onBuy.emit({product: this.product , coutProduct : this.product.cout});
        this.product.cout = this.product.cout * Math.pow(this.product.croissance, this.maxQuantity) 
        this.calcMaxCanBuy();
        this.calcTotalCost();
    }

  // calcMaxCanBuy() {
  //   let quantite: number = 0;
  //   const x = this.product.cout; // Coût actuel d'un exemplaire du produit
  //   const c = this.product.croissance; // Taux de croissance du coût du produit
  //   const argentPossede = this._money; // Argent possédé par le joueur
  
  //   // Calcul du nombre maximal de produits achetables en fonction de l'argent possédé
  //   if (this._qtmulti) {
  //     quantite = Number(this._qtmulti);
  //   } else {
  //     // Utilisation de la formule pour la somme d'une suite géométrique
  //     // Somme = x * (1 - c^quantite) / (1 - c)
  //     // On cherche la plus grande quantité telle que le coût total reste inférieur ou égal à l'argent possédé
  //     quantite = Math.floor(Math.log(1 - (argentPossede * (1 - c)) / x) / Math.log(c));
  //     if (quantite <= 0) {
  //       quantite = 1;
  //     }
  //   }
  //   this.maxQuantity = quantite; // Met à jour la quantité maximale achetable
  //   // on modifie le cout du produit ?
  //   this.product.cout = this.product.cout * Math.pow(this.product.croissance, this.maxQuantity) 

  //   return  this.maxQuantity;
  // }
  
  calcMaxCanBuy() {
    let quantite: number = 0;
    const x = this.product.cout; // Coût actuel d'un exemplaire du produit
    const c = this.product.croissance; // Taux de croissance du coût du produit
    const argentPossede = this._money; // Argent possédé par le joueur
    
    // Calcul du nombre maximal de produits achetables en fonction de l'argent possédé
    if (this._qtmulti) {
      quantite = Number(this._qtmulti);
    } else {
      // Utilisation de la formule pour la somme d'une suite géométrique
      // Somme = x * (1 - c^quantite) / (1 - c)
      // On cherche la plus grande quantité telle que le coût total reste inférieur ou égal à l'argent possédé
      quantite = Math.floor(Math.log(1 - (argentPossede * (1 - c)) / x) / Math.log(c));
      if (quantite <= 0) {
        quantite = 1;
      }
    }
    this.maxQuantity = quantite; // Met à jour la quantité maximale achetable
    
    // Calcul du coût total en utilisant la formule de la somme d'une suite géométrique
    // Somme = x * (1 - c^quantite) / (1 - c)
    this.maxCout = x * (1 - Math.pow(c, quantite)) / (1 - c);
    
    return this.maxQuantity;
  }
  
  

  isBuyable(): boolean {
    return this.maxQuantity > 0 && this._money >= this.product.cout;
  }


  clickIcon() {
    if(this.product.timeleft == 0) {
      this.restService.lancerProductionProduit(this.product).catch(reason =>
        console.log("Erreur : " + reason)
      );

      this.product.timeleft = this.product.vitesse;
      this.run = true;
      this._world.lastupdate = Date.now().toString();
    }
  }


  @Output()
  notifyProduction: EventEmitter<{product: Product, nbProduction: number}> = new EventEmitter<{product: Product, nbProduction: number}>();

  @Output()
  onBuy: EventEmitter<{product: Product, coutProduct: number}> = new EventEmitter<{product: Product, coutProduct: number}>();



  constructor(private restService: WebserviceService) {
    this.server = restService.server;
  }

  calcTotalCost() {
    let totalCost: number;
    if (this._qtmulti === 'Max') {
      totalCost = this.product.cout * (Math.pow(this.product.croissance, this.product.quantite) - 1) / (this.product.croissance - 1);
    } else {
      const quantity = Number(this._qtmulti);
      totalCost = this.product.cout * (Math.pow(this.product.croissance, quantity) - 1) / (this.product.croissance - 1);
    }
  
    return totalCost;
  }
  

  startFabrication() {
    if(this.product.timeleft == 0) {
      this.restService.lancerProductionProduit(this.product).catch(reason =>
        console.log("Erreur : " + reason)
      );
      this.product.timeleft = this.product.vitesse;
      this.run = true;
      this.product.lastupdate = Date.now().toString();
    }
  }



  calcUpgrade(palier: Palier) {
    palier.unlocked = true;
    if (palier.typeratio == "VITESSE") {
      this.product.vitesse = this.product.vitesse / palier.ratio;
    }
    else if (palier.typeratio == "GAIN") {
      this.product.revenu = this.product.revenu * palier.ratio;
    }
    else{
      this.world.angelbonus += palier.ratio;
    }
  }


  // Calculer le score
  calcScore() {
    let elapseTime: number = 0;
    let nbProduction: number = 0;

    if (this.product.timeleft != 0 || this.product.managerUnlocked) {
      elapseTime = Date.now() - Number(this.product.lastupdate);
      this.product.lastupdate = Date.now().toString();

      if (!this.product.managerUnlocked) {
        if (elapseTime > this.product.timeleft) {          
          nbProduction = 1;
          this.product.timeleft = 0;
          this.progressBarValue = 0;
          this.run = false;
        }
        else {
          nbProduction = 0;
          this.product.timeleft -= elapseTime;
        }
      }
      else {
        this.run = true;
        if (elapseTime > this.product.timeleft) {
          nbProduction = 1 + (elapseTime - this.product.timeleft) / this.product.vitesse;
          this.product.timeleft  = this.product.vitesse - (elapseTime - this.product.timeleft) % this.product.vitesse;
        }
        else {
          nbProduction = 0;
          this.product.timeleft -= elapseTime;
        }
      }      
      if (nbProduction > 0) this.notifyProduction.emit({product: this.product, nbProduction : nbProduction});
    }
  }

//   calcScore() {
//     let elapseTime: number = 0;
//     let nbProduction: number = 0;

//     if (this.product.timeleft !== 0) {
//       elapseTime = Date.now() - Number(this.product.lastupdate);
//       this.product.lastupdate = Date.now().toString();
      
//       this.product.timeleft -= elapseTime;

//       if (this.product.timeleft <= 0) {
//         nbProduction = 1;
//         this.product.timeleft = 0;
//         this.progressBarValue = 0;
//       } else {
//         this.progressBarValue = ((this.product.vitesse - this.product.timeleft) / this.product.vitesse) * 100;
//       }

//       if (nbProduction > 0) {
//         this.notifyProduction.emit({product: this.product, nbProduction : nbProduction});
//       }
//     }
// }







   // Lancer la production
   Onclick() {
    if(this.product.timeleft == 0) {
      this.restService.lancerProductionProduit(this.product).catch(reason =>
        console.log("Erreur : " + reason)
      );
      this.product.timeleft = this.product.vitesse;
      this.run = true;
      this._world.lastupdate = Date.now().toString();
    }
  }


}
